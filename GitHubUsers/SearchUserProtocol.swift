//
//  SearchUserProtocol.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation

protocol SearchUserProtocol {

    func retrieveUsersProtocol(results: [User])
}

 
