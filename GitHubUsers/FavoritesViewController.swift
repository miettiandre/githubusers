//
//  FavoritesViewController.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, FavoriteProtocol, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!

    //MARK: - Properties
    
    var favoriteManager:    FavoriteManager!
    var userFavorites       = [UserMO]()
    
    //MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate      = self
        tableView.dataSource 	= self

        //remove empty cells on tableView
        tableView.tableFooterView   = UIView(frame: .zero)
        //adjust rowHeight in tableView to resize automatic of cells according to their content
        tableView.rowHeight         = UITableViewAutomaticDimension

        self.favoriteManager    = FavoriteManager(favProtocol: self)
        self.navigationItem.title = "GitHub Users"
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.favoriteManager.retrieveFavorites()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - FavoriteProtocol
    
    func retrieveFavorites(favorites: [UserMO]?) {

        if favorites != nil {
            userFavorites = favorites!
            tableView.reloadData()
        }
        else {
            userFavorites.removeAll()
            tableView.reloadData()
        }
    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userFavorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoritesCell") as! FavoritesCell
        cell.configureCell(user: userFavorites[indexPath.row])
        return cell
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
////        userToDetail = self.foundedUsers[indexPath.row]
//        self.performSegue(withIdentifier: "showDetail", sender: self)
        
        // Deselect all cells in tableView
        if (self.tableView.indexPathForSelectedRow != nil) {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }

}

