//
//  SearchUserManager.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class SearchUserManager: NSObject {
    
    //MARK: - Protocol property
    
    var searchUserProtocol: SearchUserProtocol!
    
    //init method to set protocol class
    
    init(userProtocol: SearchUserProtocol) {
        
        self.searchUserProtocol = userProtocol
        super.init()
    }
    
    /*
     Here we access helper request to retrieve users by name with Alamofire request and pass results to view controller by protocol method
     **/
    
    func searchByName(name: String) {
        let helper = RequestHelper.sharedInstance
        helper.searchByName(name: name) { (results, err) in
            self.searchUserProtocol.retrieveUsersProtocol(results: results)
        }
    }
}
