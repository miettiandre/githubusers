//
//  UserDetailViewController.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit
import Kingfisher

class UserDetailViewController: UIViewController, UserDetailProtocol {
    
    //MARK: - Outlets
    
    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userScore: UILabel!
    @IBOutlet weak var userRepo: UIButton!
    
    //MARK: - Properties
    
    var userObject: User!
    var favButton: UIBarButtonItem!
    var detailManager: UserDetailManager!
    var repositories = [Repositories]()
    
    //MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.detailManager = UserDetailManager(detailProtocol: self)
        setup()
        setupFavoriteButton()
        self.navigationItem.title = "GitHub Users"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if FavoriteDataManager.checkFavorite(favoriteId: Int64(userObject.userId)) == true {
            favButton.image = UIImage(named: "ic_favorite_filled")
        }
        else {
            favButton.image = UIImage(named: "ic_favorite")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userPicture.layer.cornerRadius = userPicture.layer.frame.size.width / 2
        userPicture.layer.masksToBounds = true
    }
    
    func setup() {
        
        let pictureUrl = URL(string: userObject.pictureUrl)
        userPicture.kf.setImage(with: pictureUrl)
        userName.text = "User: " + userObject.name
        userScore.text = "Score: " + String(userObject.score)
        
        self.detailManager.retrieveUserRepositories(url: userObject.reposUrl)
    }
    
    func setupFavoriteButton() {
        favButton = UIBarButtonItem(image: UIImage(named: "ic_favorite"), style: .plain, target: self, action: #selector(UserDetailViewController.favoriteButtonPressed))
        self.navigationItem.setRightBarButton(favButton, animated: false)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showRepositories" {
            if let repositories = segue.destination as? RepositoriesViewController {
                repositories.userName = userObject.name
                repositories.repositories = self.repositories
            }
        }
    }
    
    //MARK: - Functional methods
    
    func favoriteButtonPressed() {
        self.detailManager.toggleFavoriteUser(user: userObject)
    }
    
    //MARK: - Actions
    
    /*
     Here we open web page with user repository on GitHub
     **/
    
    @IBAction func userRepoButtonPressed(_ sender: Any) {
        let repoUrl = URL(string: userObject.url)!
        UIApplication.shared.open(repoUrl)
    }
    
    @IBAction func listMyRepositoriesButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "showRepositories", sender: self)
    }
    
    //MARK: - UserDetailProtocol
    
    func favoriteHasBeesToggled(result: Bool?) {
        if result == true {
            self.favButton.image = UIImage(named: "ic_favorite_filled")
        }
        else {
            self.favButton.image = UIImage(named: "ic_favorite")
        }
        
    }
    
    func retrieveRepositories(repositories: [Repositories]) {
        self.repositories = repositories
    }
    
}
