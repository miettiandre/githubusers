//
//  FavoritesMO+CoreDataClass.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation
import CoreData


public class FavoritesMO: NSManagedObject {

    @NSManaged public var favoriteId: Int64
    @NSManaged public var favoriteName: String?
    @NSManaged public var favoriteUrl: String?
    @NSManaged public var favoriteAvatarUrl: String?
    @NSManaged public var favoriteScore: Int64
}
