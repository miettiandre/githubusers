//
//  UserDetailProtocol.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation

protocol UserDetailProtocol {
 
    func favoriteHasBeesToggled( result: Bool?)
    func retrieveRepositories(repositories: [Repositories])
}
