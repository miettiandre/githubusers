//
//  RequestHelper.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RequestHelper: NSObject {
    
    static let sharedInstance = RequestHelper()
    var alamofireManager : SessionManager
    
    override init() {
        
        //Set timeout in AlamofireManager
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10// seconds
        configuration.timeoutIntervalForResource = 10
        
        alamofireManager = SessionManager(configuration: configuration)
    }
    
    
    func searchByName (name: String, completion: @escaping (_ user: [User], _ error: NSError?) -> Void) {
        
        let parameter = [
            "q": name
        ]
        let urlSearch = "https://api.github.com/search/users"
        
        alamofireManager.request(urlSearch, method: .get, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            var results = [User]()
            let data_obj = JSON(data: response.data!)
            for (_, subJson):(String, JSON) in data_obj["items"] {
                let json = subJson.rawString()
                let mapObj = User(JSONString: json!)
                results.append(mapObj!)
            }
            completion(results, nil)
        }
    }
    
    func searchRepositoriesName (repoUrl: String, completion: @escaping (_ repos: [Repositories], _ error: NSError?) -> Void) {
        
        let urlSearch = repoUrl
        
        alamofireManager.request(urlSearch, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in

            var results = [Repositories]()
            let data_obj = JSON(data: response.data!)
            for (_, subJson):(String, JSON) in data_obj {
                
                let json = subJson.rawString()
                let mapObj = Repositories(JSONString: json!)                
                results.append(mapObj!)
            }
            completion(results, nil)
        }
    }
}
