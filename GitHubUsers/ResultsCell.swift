//
//  ResultsCell.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit
import Kingfisher

class ResultsCell: UITableViewCell {

    @IBOutlet weak var resultName: UILabel!
    @IBOutlet weak var userPicture: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(user: User) {
        resultName.text = user.name
        let url = URL(string: user.pictureUrl)
        userPicture.kf.setImage(with: url)
    }
}
