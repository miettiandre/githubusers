//
//  FavoritesCell.swift
//  GitHubUsers
//
//  Created by andre mietti on 16/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class FavoritesCell: UITableViewCell {

    @IBOutlet weak var favoriteName: UILabel!
    @IBOutlet weak var favoriteImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(user: UserMO) {
        favoriteName.text = user.name
        let url = URL(string: user.avatarUrl!)
        favoriteImage.kf.setImage(with: url)
    }
}
