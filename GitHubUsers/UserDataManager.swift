//
//  FavoriteDataManager.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit
import CoreData

class FavoriteDataManager: NSObject {
    
    class func saveFavoriteData(user: User, completion:(Bool?) -> Void) {
        
        let moc = DataController.sharedInstance.managedObjectContext
        
        do {
            
            let entity = NSEntityDescription.insertNewObject(forEntityName: "User", into: moc) as! UserMO
            do {
                entity.name = user.name
                entity.avatarUrl = user.pictureUrl
                entity.url = user.url
                entity.userScore = Int64(user.score)
                entity.userId = Int64(user.userId)
                try moc.save()
                completion(true)
            }
            catch let error as NSError {
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    class func removeFavorite(favoriteId: Int64, completion:(Bool?) -> Void) {
        let moc = DataController.sharedInstance.managedObjectContext
        let favoriteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        do {
            let predicate = NSPredicate(format: "userId == %d", favoriteId)
            favoriteFetch.predicate = predicate
            let fetchRequest = try moc.fetch(favoriteFetch) as! [UserMO]
            
            for item in fetchRequest {
                if item.userId == favoriteId {
                    moc.delete(item)
                    completion(true)
                }
                else {
                    completion(false)
                }
            }
        }
        catch let error as NSError {
            fatalError("error on fetche: \(error)")
        }
        completion(false)
    }
    
    class func checkFavorite(favoriteId: Int64) -> Bool? {
        
        let moc = DataController.sharedInstance.managedObjectContext
        let favoriteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        do {
            let predicate = NSPredicate(format: "userId == %d", favoriteId)
            favoriteFetch.predicate = predicate
            let fetchRequest = try moc.fetch(favoriteFetch) as! [UserMO]
            
            for item in fetchRequest {
                if item.userId == favoriteId {
                    return true
                }
                else {
                    return false
                }
            }
        }
        catch let error as NSError {
            fatalError("error on fetche: \(error)")
        }
        return false
    }
    
    class func retrieveFavorites() -> [UserMO]? {
        
        let moc = DataController.sharedInstance.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        do {
            let favoriteFetchRequest = try moc.fetch(fetchRequest) as! [UserMO]
            if favoriteFetchRequest.count > 0 {
                return favoriteFetchRequest
            }
            else {
                return nil
            }
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
