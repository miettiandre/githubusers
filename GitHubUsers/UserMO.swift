//
//  UserMO+CoreDataClass.swift
//  GitHubUsers
//
//  Created by andre mietti on 16/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation
import CoreData


public class UserMO: NSManagedObject {

    @NSManaged public var name: String?
    @NSManaged public var url: String?
    @NSManaged public var avatarUrl: String?
    @NSManaged public var userId: Int64
    @NSManaged public var userScore: Int64
}
