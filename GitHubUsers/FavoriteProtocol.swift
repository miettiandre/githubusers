//
//  FavoriteProtocol.swift
//  GitHubUsers
//
//  Created by andre mietti on 16/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation

protocol FavoriteProtocol {
    func retrieveFavorites(favorites: [UserMO]?)
}
