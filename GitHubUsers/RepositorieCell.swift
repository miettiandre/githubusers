//
//  repositorieCell.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class RepositorieCell: UITableViewCell {

    @IBOutlet weak var repositoryTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
