//
//  PersonalManager.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation
import ObjectMapper

/*
 Here we map user information with ObjectMapper
 **/

struct User: Mappable {
    
    var name        = ""
    var url         = ""
    var pictureUrl  = ""
    var reposUrl    = ""
    var userId      = 0
    var score   	= 0
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        userId      <- map["id"]
        name 		<- map["login"]
        url     	<- map["html_url"]
        score   	<- map["score"]
        pictureUrl  <- map["avatar_url"]
        reposUrl    <- map["repos_url"]
    }
}

