//
//  RepositoriesViewController.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    
    var userName: String!
    var repositories = [Repositories]()
    
    //MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "GitHub Users"

//        self.title                  = userName
        tableView.delegate          = self
        tableView.dataSource        = self
        
        //remove empty cells on tableView
        tableView.tableFooterView   = UIView(frame: .zero)
        //adjust rowHeight in tableView to resize automatic of cells according to their content
        tableView.rowHeight         = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "repositorieCell") as? RepositorieCell
        cell?.repositoryTitle.text = repositories[indexPath.row].name
        
        return cell!
    }

    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repoUrl = URL(string: repositories[indexPath.row].url)
        UIApplication.shared.open(repoUrl!)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
