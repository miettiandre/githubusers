//
//  FavoriteManager.swift
//  GitHubUsers
//
//  Created by andre mietti on 16/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class FavoriteManager: NSObject {

    //MARK: - Protocol property
    
    var favoriteProtocol: FavoriteProtocol!
    
    //init method to set protocol class
    
    init(favProtocol: FavoriteProtocol) {
        
        self.favoriteProtocol = favProtocol
        super.init()
    }

    func retrieveFavorites() {
     
        let favorites = FavoriteDataManager.retrieveFavorites()
        self.favoriteProtocol.retrieveFavorites(favorites: favorites)
    }
    
}
