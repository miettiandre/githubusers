//
//  SearchUserViewController.swift
//  GitHubUsers
//
//  Created by andre mietti on 12/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchUserViewController: UIViewController, UITextFieldDelegate, SearchUserProtocol, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet weak var searchResultsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    //MARK: - Properties
    
    var userManager: SearchUserManager!
    var foundedUsers = [User]()
    var userToDetail: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "GitHub Users"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        // Do any additional setup after loading the view.
        searchTextField.delegate = self
        self.userManager = SearchUserManager(userProtocol: self)
        searchResultsLabel.text = ""
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.rowHeight         = UITableViewAutomaticDimension
     
        //remove empty cells on tableView
        tableView.tableFooterView   = UIView(frame: .zero)
        //adjust rowHeight in tableView to resize automatic of cells according to their content
        tableView.rowHeight         = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            if let detail = segue.destination as? UserDetailViewController {
                detail.userObject = userToDetail
            }
        }
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        foundedUsers.removeAll()
        tableView.reloadData()
        let param = textField.text?.trimmingCharacters(in: .whitespaces)
        if (param?.characters.count)! > 0 {
            self.userManager.searchByName(name: textField.text!)
            SVProgressHUD.show()
        }
        else {
            textField.placeholder = "type a name"
            searchResultsLabel.text = ""
        }
        return true
    }
    
    //MARK: - SearchUserProtocol
    
    func retrieveUsersProtocol(results: [User]) {
        
        SVProgressHUD.dismiss()
        if results.count > 0 {
            foundedUsers = results
            tableView.reloadData()
            searchResultsLabel.text = "Results for: " + searchTextField.text!
        }
        else {
            searchResultsLabel.text = "None results for: " + searchTextField.text!
        }
    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foundedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultsCell") as! ResultsCell
        cell.configureCell(user: foundedUsers[indexPath.row])
        return cell
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        userToDetail = self.foundedUsers[indexPath.row]
        self.performSegue(withIdentifier: "showDetail", sender: self)
        
        // Deselect all cells in tableView
        if (self.tableView.indexPathForSelectedRow != nil) {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    
}
