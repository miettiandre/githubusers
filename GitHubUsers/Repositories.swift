//
//  Repositories.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import Foundation
import ObjectMapper

/*
 Here we map user repositories information with ObjectMapper
 **/

struct Repositories: Mappable {
    
    var name        = ""
    var url         = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        name 		<- map["name"]
        url     	<- map["html_url"]
    }
}
