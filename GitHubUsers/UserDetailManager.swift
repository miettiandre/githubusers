//
//  UserDetailManager.swift
//  GitHubUsers
//
//  Created by andre mietti on 14/04/17.
//  Copyright © 2017 andre mietti. All rights reserved.
//

import UIKit

class UserDetailManager: NSObject {
    
    //MARK: - Protocol property
    
    var userDetailProtocol: UserDetailProtocol!
    
    //init method to set protocol class
    
    init(detailProtocol: UserDetailProtocol) {
        
        self.userDetailProtocol = detailProtocol
        super.init()
    }
    
    /*
     Here we access coredata to set favorite or not favorite user and pass status to ViewController via protocol
     **/
    
    func toggleFavoriteUser(user: User) {
        
        if FavoriteDataManager.checkFavorite(favoriteId: Int64(user.userId)) == false {
            FavoriteDataManager.saveFavoriteData(user: user) { (success) in
                self.userDetailProtocol.favoriteHasBeesToggled(result: success)
            }
        }
        else {
            FavoriteDataManager.removeFavorite(favoriteId: Int64(user.userId), completion: { (success) in
                self.userDetailProtocol.favoriteHasBeesToggled(result: success)
            })
        }
    }
    
    /*
     Here we do a web request to retrieve the Url's of user repositories and pass the response to ViewController via protocol
     **/
    
    func retrieveUserRepositories(url: String) {
        
        let helper = RequestHelper.sharedInstance
        helper.searchRepositoriesName(repoUrl: url) { (results, error) in
            if error == nil {
                self.userDetailProtocol.retrieveRepositories(repositories: results)
            }
        }
    }
}
